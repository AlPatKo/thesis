\thispagestyle {empty}
\contentsline {section}{List of figures}{10}
\contentsline {section}{Listings}{11}
\contentsline {section}{Abbreviations}{12}
\contentsline {section}{Introduction}{13}
\contentsline {section}{\numberline {1}Domain Analysis}{15}
\contentsline {subsection}{\numberline {1.1}Problem description}{15}
\contentsline {subsubsection}{\numberline {1.1.1}Learning model}{15}
\contentsline {subsubsection}{\numberline {1.1.2}Factors influencing evaluation process}{16}
\contentsline {subsubsection}{\numberline {1.1.3}Assessment classification}{17}
\contentsline {subsection}{\numberline {1.2}Existing solutions}{19}
\contentsline {subsubsection}{\numberline {1.2.1}Moodle}{19}
\contentsline {subsubsection}{\numberline {1.2.2}Blackboard Learn}{20}
\contentsline {subsubsection}{\numberline {1.2.3}Google Forms}{22}
\contentsline {subsection}{\numberline {1.3}Suggested solution}{23}
\contentsline {subsubsection}{\numberline {1.3.1}Psychology behind proposed approach}{23}
\contentsline {subsubsection}{\numberline {1.3.2}System definition}{24}
\contentsline {section}{\numberline {2}System Architecture and User Interaction}{26}
\contentsline {subsection}{\numberline {2.1}System entities}{26}
\contentsline {subsection}{\numberline {2.2}User interaction}{29}
\contentsline {subsection}{\numberline {2.3}System behavior}{33}
\contentsline {subsection}{\numberline {2.4}System components}{35}
\contentsline {section}{\numberline {3}Used Technologies and System Implementation}{37}
\contentsline {subsection}{\numberline {3.1}Technologies}{37}
\contentsline {subsubsection}{\numberline {3.1.1}Ruby}{37}
\contentsline {subsubsection}{\numberline {3.1.2}Ruby on Rails}{38}
\contentsline {subsubsection}{\numberline {3.1.3}PostgreSQL}{39}
\contentsline {subsubsection}{\numberline {3.1.4}Libraries and gems}{40}
\newpage 
\contentsline {subsection}{\numberline {3.2}Implementation}{41}
\contentsline {subsubsection}{\numberline {3.2.1}Users}{42}
\contentsline {subsubsection}{\numberline {3.2.2}Tests and questions}{45}
\contentsline {subsubsection}{\numberline {3.2.3}Answer evaluation and grading}{47}
\contentsline {section}{\numberline {4}Economic Analysis}{49}
\contentsline {subsection}{\numberline {4.1}Project description}{49}
\contentsline {subsection}{\numberline {4.2}Time schedule}{49}
\contentsline {subsubsection}{\numberline {4.2.1}Objective determination and SWOT analysis}{49}
\contentsline {subsubsection}{\numberline {4.2.2}Time schedule establishment}{51}
\contentsline {subsection}{\numberline {4.3}Economic motivation}{51}
\contentsline {subsubsection}{\numberline {4.3.1}Tangible and intangible asset expenses}{52}
\contentsline {subsubsection}{\numberline {4.3.2}Salary expenses}{53}
\contentsline {subsection}{\numberline {4.4}Individual salary}{54}
\contentsline {subsubsection}{\numberline {4.4.1}Indirect expenses}{55}
\contentsline {subsubsection}{\numberline {4.4.2}Wear and depreciation}{55}
\contentsline {subsubsection}{\numberline {4.4.3}Product cost}{57}
\contentsline {subsubsection}{\numberline {4.4.4}Economic indicators and results}{57}
\contentsline {subsection}{\numberline {4.5}Economic conclusions}{58}
\contentsline {section}{Conclusions}{59}
\contentsline {section}{References}{61}
