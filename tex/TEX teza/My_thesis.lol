\contentsline {lstlisting}{\numberline {3.1}Java reading from file}{38}
\contentsline {lstlisting}{\numberline {3.2}Ruby reading from file}{38}
\contentsline {lstlisting}{\numberline {3.3}Database configuration file}{41}
\contentsline {lstlisting}{\numberline {3.4}Example of a base controller}{42}
\contentsline {lstlisting}{\numberline {3.5}Teacher model}{42}
\contentsline {lstlisting}{\numberline {3.6}Routes used by application}{44}
\contentsline {lstlisting}{\numberline {3.7}Student registrations controller}{44}
\contentsline {lstlisting}{\numberline {3.8}Course view in student namespace}{45}
\contentsline {lstlisting}{\numberline {3.9}Question creation view in teacher namespace}{45}
\contentsline {lstlisting}{\numberline {3.10}Question base}{46}
\contentsline {lstlisting}{\numberline {3.11}Multiple choice question model}{47}
\contentsline {lstlisting}{\numberline {3.12}Rendering random question from a group}{47}
\contentsline {lstlisting}{\numberline {3.13}Parsing and evaluation of student-submitted answers}{48}
