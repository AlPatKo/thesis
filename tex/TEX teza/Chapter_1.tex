\section{Domain Analysis}
\phantomsection

\subsection{Problem description}
\subsubsection{Learning model}
The role of education cannot be underestimated in modern society, and therefore requires a lot of research and resources in order to provide necessary means for its, domain's evolution, ensuring humankind's prosperity.

Nowadays, there are countless already existing methodologies on how to organize educational process in a most effective and efficient manner and many more being developed  every day. One of them is Bloom's Taxonomy. Originally elaborated in 1956 by a committee of experts in domains of education and psychology and named after Benjamin Bloom, it is a framework with the purpose of categorizing educational goals. As can be seen in figure \ref{fig:bloom}, Bloom's Taxonomy is a layered model that represents how, ideally process of learning should be structured. It consists of six layers:
\begin{itemize}
  \item[--] \textbf{Knowledge.}
    Otherwise known as \enquote{Remembering} phase. During this phase of learning, scholars are supposed to remember facts and universal specifics, recall methods, processes, and patterns.
  \item[--] \textbf{Comprehension.}
    Following the accumulation of the information about environment or any specific topic is the phase of understanding obtained data. At this stage student is able to understand and comprehend gathered knowledge, and therefore can make use of the communicated idea in prospect.
  \item[--] \textbf{Application.}
    This stage makes use of the knowledge and ideas from the preceding stages. At this point one can abstract the general point of some piece of knowledge, and apply it for similar, and not so much problems. This is the first phase that involves experience of the scholar.
  \item[--] \textbf{Analysis.}
    This phase involves students to \enquote{breakdown} rules, facts, and patterns into basic building blocks, therefore enabling them to analyze those blocks at the smallest scale, so that they can be used to construct new patterns and rules. Once this phase is reached, students can generalize the facts.
  \item[--] \textbf{Synthesis.}
    Synthesis implies ability to combine preciously retrieved knowledge, or rather building blocks of that knowledge, such as facts and rules in order to devise new patterns and processes, therefore forming a new piece of knowledge.
  \item[--] \textbf{Evaluation.}
    This phase endorses critical thinking regarding obtained knowledge. It includes such processes as evaluation of the validity of the information gathered, quality of the execution, and presence of the flaws in reasoning.
\end{itemize}

Although Bloom's methodology has been revised over the years\textsuperscript{\cite{bloom}}, generally it still holds true and is applied by the vast majority of education experts worldwide. The general idea behind it, is that each consecutive phase requires experience and knowledge from the preceding phase, the layer from below.s

\begin{figure}[ht!]
  \centering
    \includegraphics[scale=.65]{1_bloom}
  \caption{Bloom's taxonomy}
  \label{fig:bloom}
\end{figure}

\subsubsection{Factors influencing evaluation process}
The process of evaluation of one's true knowledge and skills has always been one of the crucial problems in education. This task can be quite difficult due to a myriad of categories of factors, such as, for example:

\begin{itemize}
  \item[--] \textbf{Medium based.}
    These depict the factors related to the form in which an evaluation might take place, namely, an evaluation can be written, oral, skill-based, etc. Each of these mediums has both benefits and limitations, e.g. written test allow testees to express their thoughts on the matter quite fully and elaborately, however it does not provide an objective opportunity to check the reaction time, because of the overhead in transition from thought to word explained in the following factor family, that is minimized in case of the skill tests, that involve real-time reaction and action. Depending on the particularity of the competence that is being tested, one of these mediums can be chosen or rather a combination of them, in order to provide a perspective from several angles.
  \item[--] \textbf{Human reason and interpretation.}
    This is a fundamental issue in human communication. As Lev Semyonovich Vygotsky, a Soviet psychologist and the founder of a theory of human cultural and bio-social development pointed out in his work about human thinking and speech\textsuperscript{\cite{vygotsky}}, speech is a complex process and can be split up into two different kinds: 
    \begin{itemize}
      \item Inner
      \item External
    \end{itemize}

      Inner speech being the process of transforming of input sound data to comprehensible thought objects and the following linking and understanding of those, whereas external speech is a process reverse to that of the inner speech, that involves materialization and objectivization of the thought in order to produce coherent word projection of the thoughts. Because of this type of factor humans rarely can achieve a high degree of understanding each other.
  \item[--] \textbf{Object of evaluation.}
    Sometimes the problem of understanding a question is a problem of the question itself. Due to the fact that no question is the same, and that it covers a different topic than any other question, it may be an obtrusion for a testee to be able to comprehend the essence of the question itself in the first place. This is exactly why different competences are tested in different ways, meaning that most of competences rely on a complex if not unique set of very particular skills, and hence cannot be assessed in the same manner as another skill, that require a different set of abilities.
  \item[--] \textbf{Examinator's bias.}
    As obvious as it may seem, no person is an island, and the examinator is no exception. Throughout our conscious life we tend to build all kinds of relations, some of them being long lastinf, and some being relatively short lived. This factor is extremely apparent when there is a judging mechanic involved in assessment, meaning personal appreciation from a committee of juries of a testee's abilities. Even when seeing a person for the first time we tend to project this new link through the prism of past experience, and therefore applying strongly established stereotype, that may cause an deviation in objectivity of out judgment, and hence yield wrong results.
\end{itemize}

The aforementioned categories are far from being a full and exhausting list of all the kinds of factors that have to be considered when preparing an assessment procedure, however they tend to be ones of the most important or rather obvious and comprehensible by examinators.
\subsubsection{Assessment classification}
Evaluation is a key element in one's learning, since it provides an estimate on one's learning potential and degree of capability to perform any task. Generally, assessment is believed to be of three types, as can be seen in the figure \ref{fig:assessment}.

\begin{figure}[ht!]
  \centering
    \includegraphics[scale=.8]{1_assessment}
  \caption{Assessment types}
  \label{fig:assessment}
\end{figure}

Diagnostic assessment usually takes place before learning process, and is designed for tutors to gather information about students, their current level of knowledge of a subject, their skills and capabilities. This kind of assessment is very useful for teachers, since it can provide background information of class in general, and therefore help plan learning activities, so that maximum efficiency can be achieved. This type of assessment is represented by pretests, interviews, self-assessments and discussion boards.

Formative assessment is a way to continuously analyze and evaluate students. It takes place during learning process. The process of formative assessment is aimed to gather feedback on current state of students as well as on the overall quality of classes, including evaluation of professor's methods in particular circumstances. For example, teacher may come up with an activity during classes that allows him or her to estimate students' progress, while also, through interaction get feedback on the activity itself, whether or not it should be applied in future, or if it has to be adjusted to students furthermore. Formative evaluation can be observed in question and answer sessions, homework exercise reviews and consultations, conferences, and observations during in-class activities.

Summative assessment takes place after the learning has been completed and provides information and feedback that sums up the teaching and learning processes. This type is represented by exam papers, projects and skill performances, usually at the end of the study year or semester; it is more \enquote{final product} oriented, since it provides a final grade marking one's state, whereas formative type is aimed to adjust learning process, to revise drawbacks in order to achieve highest result. Generally, after summative assessment there are no revisions. 

Furthermore, tests can be divided into two large groups:

\begin{itemize}
  \item[--] \textbf{Norm-referenced.}
    NRT aim to find a niche for individual in a population, based on a score achieved during evaluation. A common example would be SAT, where a test defines what opportunities are open for one or another student. The goal is to rank the set of examinees so that decisions about their opportunity for success (e.g. college entrance) can be made.
  \item[--] \textbf{Criterion-referenced.}
    CRT differ in that each testee’s performance is compared to a pre-defined set of criteria or a standard. The goal with these tests is to determine whether or not the candidate has the demonstrated mastery of a certain skill or set of skills. These results are usually “pass” or “fail” and are used in making decisions about job entry, or certification. 
\end{itemize}

\subsection{Existing solutions}
Nowadays, in the age of development and advancement of ICT  a great variety of platforms has emerged, including targeted towards education system, aimed at simplification and facilitation of learning process. Some of them better, some of them worse, however all of them having their benefits and drawbacks. Since this particular product has as its aim specifically to provide a platform for test management and student evaluation, there are not many analogues that occupy this specific niche, however similar services exist, mostly they are learning management systems. 

\subsubsection{Moodle}
One of such  LMS is Moodle is an acronym, standing for modular object-oriented dynamic learning environment. As can be understood from its name, this is a highly complex system, that consists of a multitude of inter-operating modules. Originally developed by Martin Dougiamas in 2001, and released for the first time in august 2002, Moodle became quite popular and gathered partners quite fast. Moodle is a very complex system, including such features as:

\begin{itemize}
  \item[--] Grade management;
  \item[--] Student assessment;
  \item[--] File sharing;
  \item[--] User messaging and forum, and many other.
\end{itemize}

No system is ideal, and Moodle, as any other system it has its own advantages and disadvantages.

As a result of both surveying its users and drawing conclusions from plain facts, the followwing\textbf{advantages} can be noted:
\begin{itemize}
  \item[--] It is Open Source, meaning the deployment is cheap, relative to other LMS systems considered
  \item[--] There is a big community who can offer advice on various topics, as well as it is highly documented
  \item[--] It offers customizable profiles
  \item[--] It has a consistent interface, meaning it does not change dramatically from one version to another
  \item[--] Customized course import, implies that one can select the content that can be carried over to a new course
  \item[--] Moodle is highly customizable, with over 1000 plugins available, a very transparent API, and customizations available directly inside the platform, such as language, themes, and structure
  \item[--] Moodle allows faculty members to organize their courses either by week or by topic so students can access their course materials in a logical chronological order
  \item[--] Restrictions allow administrators manage who has access to content and at what time. This is particularly useful for controlling the flow in which course materials are accessed. Content might be available only if a student receives a passing grade on a previous assessment
  \item[--] With the ability to customize who, when, and how notifications are sent and the ability to write custom notifications, students are always kept in the know
  \item[--] It is quite established, it has been around for a long time and it has a strong base of developers
\end{itemize}

Still there are several \textbf{disadvantages} inherent to this system:
\begin{itemize}
  \item[--] Moodle releases several versions each year and makes it difficult at time to stay current
  \item[--] Learning Moodle is difficult for new users because the interface is not the most intuitive
  \item[--] Moodle does not allow learning assets to be used in more than one course which leads to storing duplicate content from time to time
  \item[--] History is lost when a student is unenrolled from a course
  \item[--] Moodle is open source, which means that it can be modified to meet one's requirements. However, the support materials and developer documentation can be lacking. It is very easy to shoot oneself in the foot while making some changes
  \item[--] Newer learning management systems may have more elegant out-of-the-box UI. Moodle has extensive design capabilities, but extensive customization can prove complicated
  \item[--] It may become quite bug infested, when a non-technical person tries to customize and/or extend base platform using a big number of plugins
\end{itemize}

Overall, Moodle is a good solution, especially for those who are comfortable with technical terms, and especially for those who seek a tool that would allow integration of third party solutions, high degree of customization and an open source one. It is free, that is one of the major facts that attract users.

\subsubsection{Blackboard Learn}
Blackboard Learn, or as it was previously known Blackboard Learning Management System is a virtual learning environment and course management system, released on January 21, 1997, and developed by Stephen Gilfus and Dan Cane. It is a Web-based server software which features course management, customizable open architecture, and scalable design that allows integration with student information systems and authentication protocols. It may be installed on local servers or hosted by Blackboard ASP Solutions. Its main purposes are to add online elements to courses traditionally delivered face-to-face and to develop completely online courses with few or no face-to-face meetings.

Blackboard Learn provides the following features:
\begin{itemize}
  \item[--] Announcements: Professors and teachers may post announcements for students to read.
  \item[--] Chat: This function allows those students who are online to chat in real time with other students in their class section.
  \item[--] Discussions: This feature allows students and professors to create a discussion thread and reply to ones already created.
  \item[--] Mail: Blackboard mail allows students and teachers to send mail to one another. This feature supports mass emailing to students in a course.
  \item[--] Course content: This feature allows teachers to post articles, assignments, videos etc.
  \item[--] Calendar: Teachers can use this function to post due dates for assignments and tests.
  \item[--] Learning modules: This feature is often used for strictly online classes. It allows professors to post different lessons for students to access.
  \item[--] Assessments: This tab allows teachers to post quizzes and exams and allows students to access them via the Internet.
  \item[--] Assignments: This features allows assignments to be posted and students to submit assignments online.
  \item[--] Grade Book: Teachers and professors may post grades on Blackboard for students to view.
\end{itemize}

This platform provides a vast variety of features and possibilities for learning management, and therefore it is quite popular among users. According to Campus Computing research from 2013, Blackboard Learn has the biggest market share of 41\% among learning management systems\textsuperscript{\cite{campus}}. However, it also has some drawbacks.

Overview of Blackboard Learn had pointed out \textbf{advantages}, including:
\begin{itemize}
  \item[--] Blackboard provides an environment of customization, personalization, and integration through the Building Blocks program. This provides an effective way to extend the capabilities of the system through the addition of third party, open source, and home grown applications.
  \item[--] Blackboard empowers all kinds of educational institutions with the ability to extend the boundaries of the classroom through content delivery, assessment, and a suite of communication tools.
  \item[--] The email service in Blackboard allows teachers to send group messages to the entire class very easily.
  \item[--] The discussion board feature of Blackboard helps facilitate group discussion and gave students who may have been shy in class or did not think of a comment in time a place to share their thoughts.
  \item[--] The grade book feature of Blackboard allows students to quickly access their progress in the course.
  \item[--] Blackboard is very easy for the students and the instructors to use. There are a lot of training materials developed by Blackboard (and by other schools), including written manuals and videos that students and instructors can watch.
  \item[--] Blackboard technical support is very helpful and quick to provide a solution when there is an issue.
  \item[--] Blackboard has the ability for teachers to share their desktop and occupy a students desktop to help students with their academics and computer problems.
\end{itemize}

As good as it may seem, system is not lacking \textbf{disadvantages}. The latter including:
\begin{itemize}
  \item[--] With the wealth of tools and capabilities, as well as Blackboard acquisitions over the years, the integration of these tools has sometimes lagged and taken longer due to competing priorities with support, new product development, and maintenance.
  \item[--] Integration with Java trigger all kinds of bugs.
  \item[--] Blackboard requires initial training for newcoming users to set up classes and incorporate their own content.
  \item[--] The mathematical calculations in the Grade Center are weak. It does not take into account the null values so the averages are sometimes skewed.
  \item[--] The browser compatibility issues need a lot of work. Some users expect to use Internet Explorer and then have a lot of issues with Blackboard.
  \item[--] This is a proprietary system, and therefore is more expensive that its counterparts.
  \item[--] Needs responsive design for mobile users. 
  \item[--] Some of the essential functionality is hidden behind \enquote{paywalls}.
  \item[--] Beyond the basics, the learning curve of Blackboard can be quite steep, especially for users who have some hesitancy with learning new technologies. Even for savvy users, the curve can be daunting when digging into some of the advanced features.
\end{itemize}

Overall, Blackboard Learn seems to be quite popular among users and educational institutions, even though it has a fair amount of issues.

\subsubsection{Google Forms}

As trivial as it may seem, Google Forms is a great service that along its many other use cases can be used for student assessment. It provides a rather restricted functionality, as it is not designed primarily to be used by educational institutions, however its charm is exactly in this feature. Most of the learning management systems tend to get bloated as they try to embrace more and more functionality, whereas Google Forms is a simple service that serves one sole purpose -- creation of surveys or tests and the following data gathering. Since it is provided alongside other products from the data representation line up of Google, Forms can be combined with those in order to analyze students' performance. 

When compared to alternatives, some \textbf{advantages} for using Forms become apparent, including:
\begin{itemize}
  \item[--] It is free of charge
  \item[--] It is quite popular, maybe not in education, but otherwise it is well known among people
  \item[--] It provides a possibility consume Google API, and therefore integrate forms into other web applications
  \item[--] It has a modern and very intuitive user interface, everything that it has can be easily accessed
  \item[--] Even if something cannot be accessed easily, there is official documentation of products, moreover Google's search engine is \enquote{trained} to render automatically documentation in case it receives a search request on that product
  \item[--] Google forms are highly customizable with a big variety of possible form items
\end{itemize}
On the other hand, there are \textbf{limitations} to this solution:
\begin{itemize}
  \item[--] Forms are not designed primarily to be used in educational purposes, therefore they offer limited functionality in terms of grade management, however in this case another product from Google suite can be used, namely sheets in order to keep track of students' progress
  \item[--] Although, forms offer a big deal of customization in terms of form fields, some functionality is lacking, thus rendering some questions impossible in terms of asking
  \item[--] Without a clearly defined file hierarchy, results may get lost and data can become messy
\end{itemize}

Frankly speaking, even though this solution is not the most elegant, it is quite simple and therefore it may be appealing to some teachers to use it as a paper-based examination substitution in scope of several classes. Due to the fact that it bears single purpose it is quite intuitive to navigate in such a system, and it makes it faster to both learn and use.
\subsection{Suggested solution}
\subsubsection{Psychology behind proposed approach}

As a student, throughout my student life, I has always felt profoundly dissatisfied by the way evaluations took place. It is always quite stressful, everyone seems to be scared and lose any kind morale. This happens mostly due to the following facts:

\begin{itemize}
  \item[--] \textbf{Application of NRTs.}
    The fact that norm referenced tests are used provokes a deep psychological fear of being rejected due to the feeling of inferiority among colleagues. When this kind of test is applied in order to judge one's performance, result that is yielded bears not only objective part of comparing one student's knowledge of the subject against a norm, but it also contains a subjective part, where due to the fact that results and overall testing procedure is public, meaning people are basically locked in the same room and are forced into distressing conditions, people tend to compare themselves to their peers, which leads to a variety of psychological issues and complexes.
  \item[--] \textbf{Evaluation in close quarters.}
    As it was mentioned before, nowadays, assessments usually bear a batch character, that is a bunch of student is seated in an auditorium with supervisors and is expected to cut off all communication. This is fundamentally wrong, since not only people are forced together and yet are apart, but also because others work as distractors. Students may find it difficult, if not impossible to concentrate while observing how others perform.
  \item[--] \textbf{Imposed restrictions.}
    Usually, when an exam is approved there is nothing a common student do, but mobilize and gather all his or her wits to try and pass it. The fact that student has no control over the date, place, and other details of exam, makes him or her feel lack of control, that leads toward more stress as a result.
\end{itemize} 

Therefore, in order to ensure higher results and objectivity of the former, it is important to adopt more criterion referenced testing rather than normative based. CRTs tend to impose less stress on testees. This can be explained quite simple, since criterion based tests are aimed to reflect whether or not a student has a minimum required performance to pass, and therefore is not forced to compare his or her results with those of peers. This duality of results leads to less confusion and stress among students.

If combined with a way of holding examinations in private, a higher degree of freedom may result in better performance and it may also lower negative psychological impact on students. Hence, online examination with semi strict deadlines is a direction that has to be explored. Not only these benefits toward students, but this way teachers may also benefit from reduced pressure and semi automated process of evaluation.

A research performed by a group of medical experts, including Singh R., Goyal M., Tiwari S., Ghildiyal A., Nattu S.M., and Das S. had shown that in little amounts stress may be beneficial to evaluation as it boosts cognitive abilities, however constant exposure to stresses caused by examinations may result in neuropsychiatric illnesses like anxiety and depression\textsuperscript{\cite{stress}}. Not only that, but preparing for an exam is often accompanied by extended consume of caffeine, alcohol, and exhaustive one-nighters, that in their turn bear additional negative impact on student's psyche.

\subsubsection{System definition}

After analyzing previous sections, some key aspects were defined for the system. In order to prove efficient and competition worthy system has to possess a combination of both higher performance metrics than those of competitor solutions and innovative, or rather scientific approach and argumentation in devising a user interface.

The system that is described in this work will adopt the following qualities and features:
\begin{itemize}
  \item[--] \textbf{Micro service architecture.}
    System should not try and do everything, but rather focus on several small modules, namely test creation and management, processing of student answers, and the following evaluation of those answers. This way system will not get bloated over time, and could provide an API, that will allow its further integration into other tools.
  \item[--] \textbf{Simplistic and intuitive interface.}
    Application interface must be designed with the thought in mind, that it can be used by non tech savvy persons, and hence should provide intuitive way for users to interact with the system. Moreover, interface should not be bloated with radical design decisions in order not to confuse its users.
  \item[--] \textbf{Have a well defined hierarchy.}
    Although system treats design customization more as of blows and whistles, than actual functionality, it should provide a means for faculty to define course structure in a clear and concise manner.
\end{itemize}

At this moment system is in its working prototype stage, meaning it possesses some of assumed functionality. Current system's snapshot allows faculty administrators to define hierarchy of the institution, i.e. it provides such management features for:

\begin{itemize}
  \item[--] Faculties within an educational institution;
  \item[--] Teacher within a faculty;
  \item[--] Specialities covered by the former faculties;
  \item[--] Courses taught in the curricula of specialities;
  \item[--] Groups defined in the scope of specialities.
\end{itemize}

Once registered in the system, teachers can authenticate into platform and manage courses assigned to them. This feature includes editing of the syllabus, as well as creation and management of the tests in scope of a course. In addition, teacher has access to the history of taken tests, meaning he or she can see the score one student accumulated, as well as see the answers that were given.

From the student's point of view, user has access to the information regarding courses assigned to group, of which current student is a member. Student has a possibility to take tests, and see the score accumulated.

Since, it is a quite early version of the system it has a fairly  limited functionality. For example, at this point there are only three types of questions supported by test creating mechanic, namely: essay questions, that require an elaborate rumination on a specific topic; multiple choice questions, where examinator provides a set of possible answers, where only one is correct; and finally, validity questions, or otherwise known as \enquote{true or false} questions, where tutor provides a statement and asks student whether or not it is valid. The process of answer evaluation is semi automatic, meaning validity and multiple choice questions are automatically checked by the system, whereas answers on essay questions require professor's revision.

If adopted and delegated to a team of developers, system has a potential and can become a viable substitute to LMS, especially in institutions that do not require extended list of features provided by existing learning management systems, but would rather rely on narrow targeted solutions.


\clearpage