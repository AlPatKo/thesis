\section{Used Technologies and System Implementation}
\phantomsection
\subsection{Technologies}
Before the actual development of any application starts, developer has to clearly understand what has to be done and which way of executing it is the optimal regarding time consumption, performance metrics, and not in the least, readability and maintainability of the application further in time. After taking into consideration these factors it was decided to make student assessment platform a web application using Ruby on Rails web framework with the code base being written mostly in Ruby.
\subsubsection{Ruby}
Ruby is a dynamic, open source programming language developed by Yukihiro \"Matz\" Matsumoto and released in 1995. According to its creator, Ruby was influenced by Perl, Smalltalk, Eiffel, Ada, and Lisp\textsuperscript{\cite{ruby}}. Ruby supports multiple programming paradigms, including OOP, functional programming, and imperative programming. In Ruby, everything is an object. Every bit of information and code can be given their own properties and actions. Object-oriented programming calls properties by the name instance variables and actions are known as methods. It is very easy to write a class in Ruby.

Ruby is a very easy to learn language, not only that but it also possesses a list of quite appealing features:
\begin{itemize}
  \item[--] Ruby has a clean and easy syntax that allows a new developer to learn Ruby very quickly and easily. The code written in Ruby is very similar to natural language and therefore can be easily understood
  \item[--] Ruby has a rich set of built-in functions, which can be used directly into Ruby scripts.
  \item[--] Ruby and its core libraries are written in C, which makes programs written in Ruby have great performance.
  \item[--] In Ruby, class bodies aren't a special context. They're simply a context where self points at the class object.
  \item[--] They make it possible to implement block-scoped constructs that look like language features. The most common example usage is for File operations can be seen in the following snippets. 
\end{itemize}
\begin{center}
\begin{minipage}{.48\textwidth}
  \lstinputlisting[breaklines, language=Java, caption=Java reading from file]{../source/file.java}
\end{minipage}
\hfill
\begin{minipage}{.48\textwidth}
  \lstinputlisting[breaklines, language=Ruby, caption=Ruby reading from file]{../source/file.rb}
\end{minipage}
\end{center}
\subsubsection{Ruby on Rails}
Ruby on Rails, or simply Rails, is a web application framework written in Ruby under the MIT License. Rails is a model–view–controller (MVC) framework, providing default structures for a database, a web service, and web pages. It encourages and facilitates the use of web standards such as JSON or XML for data transfer, and HTML, CSS and JavaScript for display and user interfacing. David Heinemeier Hansson first released Rails as open source in July 2004. 

Rails endorses use of some well-known paradigms and design patterns, including:

\begin{itemize}
  \item[--] \textbf{DRY.}
    In software engineering, don't repeat yourself (DRY) is a principle of software development, aimed at reducing repetition of information of all kinds, especially useful in multi-tier architectures. The DRY principle is stated as \"Every piece of knowledge must have a single, unambiguous, authoritative representation within a system.\" The principle has been formulated by Andy Hunt and Dave Thomas in their book The Pragmatic Programmer. 
  \item[--] \textbf{CoC.}
    Convention over configuration is used by software frameworks that attempt to decrease the number of decisions that a developer using the framework is required to make without necessarily losing flexibility. The concept was introduced by David Heinemeier Hansson to describe the philosophy of the Ruby on Rails web framework, but is related to earlier ideas like the concept of \"sensible defaults\" and the principle of least astonishment in user interface design.
  \item[--] \textbf{Active Record.}
    The active record pattern is an architectural pattern found in software that stores in-memory object data in relational databases. It was named by Martin Fowler in his 2003 book Patterns of Enterprise Application Architecture\textsuperscript{\cite{fowler}}. The interface of an object conforming to this pattern would include functions such as Insert, Update, and Delete, plus properties that correspond more or less directly to the columns in the underlying database table.
\end{itemize}

The MVC pattern and all of the communications between entities are reflected in the figure \ref{fig:routing}.
\begin{figure}[!h]
  \centering
    \includegraphics[scale=.55]{routing}
    \caption{Routing process of a Rails application\textsuperscript{\cite{startrails}}}
  \label{fig:routing}
\end{figure}
\begin{enumerate}[label*=\arabic*]
 \item Request is sent from the client's browser to the URI that was either followed as a link or introduced into the address bar.
 \item Rails router matches received request with an action using file \'config/routes.rb\' and parses request URI in order to pass the parameters further.
 \item Matched controller action is executed using parameters parsed from request URI.
 \item In case the action accessed previously requires communication with database, the corresponding model is invoked.
 \item Model handles all of the communication with database and returns a status, whether the transaction has been successful or not.
 \item Model returns retrieved data from the database to controller.
 \item Controller calls view corresponding to the action and handles data to the presentation layer of the application.
 \item Controller returns the HTML/XML and metadata back to the browser which gets displayed on the client's screen.
\end{enumerate}

\subsubsection{PostgreSQL}
PostgreSQL is a powerful, open source object-relational database system. It has more than 15 years of active development and a proven architecture that has earned it a strong reputation for reliability, data integrity, and correctness. PostgreSQL is cross-platform and runs on many operating systems including Linux, FreeBSD, OS X, Solaris, and Microsoft Windows. It is fully ACID compliant, has full support for foreign keys, joins, views, triggers, and stored procedures. PostgreSQL evolved from the Ingres project at the University of California, Berkeley. In 1982 the leader of the Ingres team, Michael Stonebraker, left Berkeley to make a proprietary version of Ingres\textsuperscript{\cite{postgres}}. He returned to Berkeley in 1985, and started a post-Ingres project to address the problems with contemporary database systems that had become increasingly clear during the early 1980s.

For the purposes of the system described in the work, PostgreSQL is very well suited, as it ensures consistency of the data, which is crucial in terms of keeping user data, such as student grades and answers.

\subsubsection{Libraries and gems}

When it comes to extending Ruby code and providing extra functionality, a vast majority of developers if not all resign to usage of libraries written by other member of community and referred to as \emph{gems}. Functionality included into gems may vary from simple helper functions to consuming specific APIs. This system requires the following libraries:
\begin{itemize}
  \item[--] \textbf{haml-rails.}
    This enables Haml as templating engine for Rails application, therefore greatly reducing time needed for mark up of views.
  \item[--] \textbf{pg.}
    This library enables Rails applications to use PostgreSQL as DBMS.
  \item[--] \textbf{jquery-rails.}
   This gem allows developers to use jQuery in their applications and tests.
  \item[--] \textbf{devise.}
   Devise is a flexible authentication solution for Rails based on Warden. It provides such features as database authentication, omniauth, password recovery and remembering, user registration and confirmation, session timeout.
  \item[--] \textbf{ckeditor.}
    CKEditor is an HTML text editor designed to simplify web content creation. It provides a lot of customization and functionality,
  \item[--] \textbf{thin.}
    Thin is a web server, alternative to the default WEBrick that is shipped with Rails. It provides high performance and security; a network I/O library with extremely high scalability, performance and stability, a minimal interface between web servers and Ruby frameworks.
  \item[--] \textbf{spring.}
    Spring is a Rails application preloader. It speeds up development by keeping application running in the background so that there is no need to boot it every time when running a test, raking a task or migration.
  \item[--] \textbf{pry-rails.}
    Pry is a powerful alternative to the standard IRB shell for Ruby. It features syntax highlighting, a flexible plugin architecture, runtime invocation and source and documentation browsing.
\end{itemize}
\addtocontents{toc}{\protect\newpage}
\subsection{Implementation}
Before starting write code and implement key features of the application, it has to be set up properly, starting with database configuration. In rails applications, configurations related to DBMS can be found and adjusted in file \emph{config/database.yml} contents of which can be seen in the following chunk of code. This file contains four parts: default, development, testing, and production. Default being the set of common configurations for the remaining three. The latter refer to configurations specific for environments. 

\lstinputlisting[language=XML, label=lst:db, caption= Database configuration file]{../source/database.yml}
\vspace{1em}

As can be seen, configuration file contains a list of parameters required for connection and management of database:
\begin{itemize}
  \item[--] \textbf{Adapter.} This field specifies what adapter has to be used for accessing database, in this case it is PostgreSQL.
  \item[--] \textbf{Encoding.} Tells application how to encode data.
  \item[--] \textbf{Pool.} Sets up the limit of parallel connections.
  \item[--] \textbf{Timeout.} Sets up the time limit after which connection is dropped.
  \item[--] \textbf{Username.} Specifies what Postgres user/role to select for querying.
  \item[--] \textbf{Password.} Reflects password of said user.
  \item[--] \textbf{Host.} Specifies URI for communication with database.
  \item[--] \textbf{Database.} Tells application which database to use according to current environment.
\end{itemize}
 Once application is configured properly, the process of development can commence.
\subsubsection{Users}
Due to the fact that platform is  designed to have three kinds of users with different sets of privileges, it was decided to separate all controllers and layouts into three corresponding namespaces. In rails this can be done defining a parent controller, where all the details specific to a user type are noted and then every new controller belonging to the scope of particular user has to inherit from the base controller as can be seen in snippet \ref{lst:t_cont}. Base controller has to inherit from ActionController, which is the topmost controller. It includes by default a variety of procedures, including:
\begin{itemize}
  \item[--] CSRF token validation;
  \item[--] Filtering before actions;
  \item[--] Protection of sessions from forgery.
\end{itemize}

The second line shows an example of before action filters, in this case controller is said to try and log teacher into system. This method authenticates user whose login and encrypted password can be found in parameters of request. Next line is tasked with specifying base layout for all of the views that correspond to actions of controllers that inherit from TeacherController class.
\lstinputlisting[language=Ruby, caption=Example of a base controller, label={lst:t_cont}]{../source/teacher_controller.rb}
\vspace{1em}

However, at the same time as this step is done it is necessary to create a model for Teacher, that has a structure defined in snippet \ref{lst:t_model}. As can be seen from the first line of code, this model, just as any other in this application, inherits functionality from ActiveRecord::Base. This allows developers to reduce the time needed for model definition dramatically, as ActiveRecord automatically provides access to fields in database of the designated model. Moreover, due to the concept of Convention over Configuration, most of linking between models, views and controllers is done by Rails itself, e.g. application knows by default, unless specified otherwise, that requests from Teacher model should be addressed towards teachers table in database.
\lstinputlisting[language=Ruby, caption=Teacher model, label={lst:t_model}]{../source/teacher.rb}
\vspace{1em}
Following lines describe authentication subsystem parameters for Teacher, or otherwise speaking which Devise modules should be included:
\begin{itemize}
  \item[--] \textbf{Database Authenticatable.} Hashes and stores a password in the database to validate the authenticity of a user while signing in. The authentication can be done both through POST requests or HTTP Basic Authentication.
  \item[--] \textbf{Recoverable.} Resets the user password and sends reset instructions.
  \item[--] \textbf{Rememberable.} Manages generating and clearing a token for remembering the user from a saved cookie.
  \item[--] \textbf{Trackable.} Tracks sign in count, timestamps and IP address.
  \item[--] \textbf{Validatable.} Provides validations of email and password. It's optional and can be customized, so you're able to define your own validations.
  \item[--] \textbf{Registerable}(in case of Student model). Handles signing up users through a registration process, also allowing them to edit and destroy their account.
\end{itemize}

Devise parameters are followed by declaration of relations with other entities. In case of teachers there are two has\_many relations, those state that there are multiple course entities associated with a teacher entity, as well as that teacher has test entities associated through existing courses. Next, there is declaration of mapping each and every teacher to a specific faculty. This is fairly standard for every kind of user in this application, differing only in entities from associations, administrator having none due to the fact that he or she does not depend on any other entity in the system and yet has control over, virtually, every entity. Student's associations, however, are as follows: 
\begin{itemize}
  \item[--] Has many: courses and attempts;
  \item[--] Belongs to: group.
\end{itemize}

Furthermore, both teacher and student model have an instance method returning full name of the user.

In order to ensure that URL that are accessed by users do not cause any collisions, in the \emph{config/routes.rb} file have to be defined namespaces, as well as accessible resources.

\lstinputlisting[language=Ruby, caption=Routes used by application, label={lst:routes}]{../source/routes.rb}
\vspace{1em}

Second line of routes has the purpose of mounting CKEditor resources, so that assets can be loaded in rich text editor. The next two lines are used to specify that Devise should use a customized controller for Student authentication, this happens due to the fact that on registration students have to select group they belong to, and therefore it is necessary to adjust list of permitted parameters as is shown in \ref{lst:permit}. 
\lstinputlisting[breaklines, language=Ruby, caption=Student registrations controller, label={lst:permit}]{../source/registrations_controller.rb}
\vspace{1em}
Furthermore, in routes can be seen definitions of paths for various resources, as, for example on the line 13 from listing \ref{lst:routes}. That line of code asks Rails application to create paths for all the actions, except show, the former being: index, new, create, edit, update, and destroy.

Basically, this is the concise description of how the application was separated in three layers according to end user class and granted privileges. Thus, having three different base controllers and layouts, system can be decide automatically what action to allow to current user, as well as what can and has to be presented on the screen.

\subsubsection{Tests and questions}
At this point of time it was more reasonable to implement test creation and management mechanics only in the scope of a teacher. However, both student and teacher can interact with this subsystem. Teacher can proceed to courses URL, where is displayed list of all the courses that where assigned to current teacher. Once picked a specific course, teacher is able to update syllabus of said course, or proceed to manage tests for that course. If the teacher selects to create a test, he or she will be granted with a form for test description. After submission of the latter, they will be redirected to test page, where name and description can be modified, as well as a visibility option can be set. This option allows teacher to modify test as long as it was not published for the students, since students are only able to access visible tests as is shown in snippet \ref{lst:vis}.

\lstinputlisting[breaklines, linerange={10-14}, firstnumber=10,language=HTML, caption=Course view in student namespace, label={lst:vis}]{../source/show.haml}
\vspace{.5em}
Before publishing the test, teacher may attach and edit questions in the test. When creating a new question, teacher are prompted with a dropdown that allows them to choose what kind of question it is, as can be seen in the listing below.

\lstinputlisting[breaklines, language=HTML, caption=Question creation view in teacher namespace, label={lst:vis}]{../source/new.haml}
\vspace{.5em}
As can be seen, this view relies on some JavaScript code. The lines 7 through 18 are responsible to get the value from dropdown and render a corresponding form for the type of question that was selected.

The application incorporates several types of questions. It is done using STI, that allows multiple models to access and store data in the same table, therefore projecting OOP inheritance onto database. All three types of questions inherit from the base class that can be seen in listing \ref{lst:q_base}. The second and the third lines reflect associations for question entities. The fifth line of code is used to tell database how to distinguish children questions, and it is done by specifying type column. Class method varieties provides question class with kind of questions that are possible. Further down the listing, near the end, can be seen scope lambdas, that serve the purpose of facilitating the process of addressing to children models.
\lstinputlisting[breaklines,language=Ruby, caption=Question base, label={lst:q_base}]{../source/question.rb}
\vspace{1em}
Each of the children questions has to be declared as a separate class, as can be seen in the example of MultipleChoice question in listing \ref{lst:q_mult}. The structure of the class is quite similar for all inheriting classes, with the sole exception that MultipleChoice class has an extra method that creates a set f all options for the anwer by merging multiple distractors with the correct answer into an array.
\lstinputlisting[breaklines,language=Ruby, caption=Multiple choice question model, label={lst:q_mult}]{../source/mult.rb}
\vspace{1em}
As can be seen on the lines 2 and 3, model name has to be overriden to match that of the parent class, this is required because all of the children classes are fundamentally questions, and a common ground has to be drawn between them.

\subsubsection{Answer evaluation and grading}

Student may navigate through courses that are lectured for his group and find out that he or she has a test waiting to be answered. If this is the case, student has to proceed to the test page, where some information about test is presented, including name of the test and brief description, alongside the link to a page where student can answer questions from the test. Questions in tests are grouped according to order field, which makes it possible for teachers to provide several variations of a question, from which randomly one will be selected and rendered on student's page as is reflected in snippet \ref{lst:q_sample}. 
\lstinputlisting[breaklines, linerange={2-7}, firstnumber=2,language=HTML, caption=Rendering random question from a group, label={lst:q_sample}]{../source/attempt.html}
\vspace{1em}
Once student has answered all the questions and submitted the form, AttemptsController steps in line. It has to parse received answer set, evaluate preciseness of submitted answers where possible, create corresponding entries in database, and associate those entries with the test. All of the mentioned tasks are executed in the following snippet.
\clearpage
\lstinputlisting[breaklines,language=Ruby, linerange={8-30},caption=Parsing and evaluation of student-submitted answers, label={lst:att_con}]{../source/attempts_controller.rb}
\vspace{1em}

After submission and semi automatic evaluation, if there were no essay questions in test, both Student and Teacher can view results and accumulated score per student per test, moreover in teacher's perspective they are sorted in a descending manner. However, if there were any essay questions, students are prompted with a message asking them to wait patiently for teacher to evaluate their essays, and teacher is able to review students' attempts and grade them objectively. This is possible due to the fact, thatif not graded, attempts are shown anonymously to teacher, therefore decreasing the chance of a grade based on bias.
\clearpage