class Question < ActiveRecord::Base
  belongs_to :test
  has_many :answers

  self.inheritance_column = :variety

  def self.varieties
    %w(Essay Validity MultipleChoice)
  end

  scope :essays, -> {where(variety: 'Essay')}
  scope :validities, -> {where(variety: 'Validity')}
  scope :multiple_choices, -> {where(variety: 'MultipleChoice')}
end