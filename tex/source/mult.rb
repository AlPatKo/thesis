class MultipleChoice < Question
  def self.model_name
    Question.model_name
  end

  def choices
    self.distractors.chomp.split('|') << self.answer
  end
end