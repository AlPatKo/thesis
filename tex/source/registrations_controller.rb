class Student::RegistrationsController < Devise::RegistrationsController
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up){|u| u.permit(:email, :name, :surname, :password, :password_confirmation, :group_id, :remember_me)}
  end
end
