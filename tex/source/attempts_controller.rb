class Student::AttemptsController < StudentController
  before_action :set_ancestors

  def new
    @attempt = current_student.attempts.new
  end

  def create
    @attempt = current_student.attempts.create(test_id: @test.id)
    attempt = params[:attempt]
    attempt.each do |q_id, answer|
      q = Question.find q_id
      if q.variety == 'Essay'
        @attempt.answers.create(response: answer[:response], checked: false, points: 0, question_id: q_id)
      elsif q.variety == 'MultipleChoice'
        if answer[:response] == q.answer
          @attempt.answers.create(response: answer[:response], checked: true, correct: true, points: q.worth, question_id: q_id)
        else
          @attempt.answers.create(response: answer[:response], checked: true, correct: false, points: 0, question_id: q_id)
        end
      else
        if answer[:response] == q.validity.to_s
          @attempt.answers.create(response: answer[:response], checked: true, correct: true, points: q.worth, question_id: q_id)
        else
          @attempt.answers.create(response: answer[:response], checked: true, correct: false, points: 0, question_id: q_id)
        end
      end
    end


    puts "|||||#{params}"
    redirect_to student_course_test_path(@course, @test)
  end

  protected
  def set_ancestors
    @course = Course.find params[:course_id]
    @test = Test.find params[:test_id]
  end
end
