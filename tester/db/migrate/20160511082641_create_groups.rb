class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.belongs_to :speciality, index: true
      t.date :est
      t.integer :lang

      t.timestamps null: false
    end
  end
end
