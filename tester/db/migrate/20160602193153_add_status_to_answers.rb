class AddStatusToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :checked, :boolean, default: false
  end
end
