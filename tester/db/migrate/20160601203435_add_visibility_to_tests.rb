class AddVisibilityToTests < ActiveRecord::Migration
  def change
    add_column :tests, :visible, :boolean, default: false
  end
end
