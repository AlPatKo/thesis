class CreateSpecialities < ActiveRecord::Migration
  def change
    create_table :specialities do |t|
      t.belongs_to :faculty, index: true
      t.string :full
      t.string :short
      t.text :about

      t.timestamps null: false
    end
  end
end
