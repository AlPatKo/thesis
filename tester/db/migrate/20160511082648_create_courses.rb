class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.belongs_to :teacher, index: true
      t.string :code
      t.string :full
      t.string :short
      t.text :syllabus

      t.timestamps null: false
    end
  end
end
