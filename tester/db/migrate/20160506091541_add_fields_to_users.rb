class AddFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :teachers, :name, :string
    add_column :teachers, :surname, :string
    add_column :teachers, :faculty_id, :integer

    add_column :students, :name, :string
    add_column :students, :surname, :string
    add_column :students, :group_id, :integer
  end
end
