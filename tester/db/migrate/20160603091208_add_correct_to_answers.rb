class AddCorrectToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :correct, :boolean
    add_column :answers, :points, :integer
  end
end
