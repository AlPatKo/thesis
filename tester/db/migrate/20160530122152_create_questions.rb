class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      # base
      t.integer :test_id
      t.string :variety
      t.integer :worth
      t.text :body
      t.integer :order

      # validity
      t.boolean :validity

      # multiple choice
      t.text :distractors
      t.string :answer

      t.timestamps null: false
    end
  end
end
