class AddDataToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :name, :string
    rename_column :groups, :est, :founded
  end
end
