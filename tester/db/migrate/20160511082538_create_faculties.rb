class CreateFaculties < ActiveRecord::Migration
  def change
    create_table :faculties do |t|
      t.string :full
      t.string :short
      t.text :about

      t.timestamps null: false
    end
  end
end
