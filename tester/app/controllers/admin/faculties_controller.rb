class Admin::FacultiesController < AdminController
  def index
    @faculties = Faculty.all
  end

  def new
    @faculty = Faculty.new
  end

  def create
    @faculty = Faculty.new faculty_params
    if @faculty.save
      redirect_to admin_faculties_path
    else
      render :new
    end
  end

  def edit
    @faculty = Faculty.find params[:id]
  end

  def update
    @faculty = Faculty.find params[:id]
    if @faculty.update faculty_params
      redirect_to admin_faculties_path
    else
      render :edit
    end
  end

  def destroy
    @faculty = Faculty.find params[:id]
    @faculty.destroy
    redirect_to admin_faculties_path
  end

  protected
  def faculty_params
    params.require(:faculty).permit!
  end
end
