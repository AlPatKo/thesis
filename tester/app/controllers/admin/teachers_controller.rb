require 'digest'
class Admin::TeachersController < AdminController
  before_action :set_faculty

  def new
    @teacher = @faculty.teachers.new
  end

  def create
    teacher_params.merge!(password: Digest::MD5.hexdigest(teacher_params[:email]))
    @teacher = @faculty.teachers.new teacher_params
    if @teacher.save
      redirect_to edit_admin_faculty_path(@faculty)
    else
      render :new
    end
  end

  def show
    @teacher = Teacher.find params[:id]
  end

  def destroy
    @teacher = Teacher.find params[:id]
    @teacher.destroy
    redirect_to edit_admin_faculty_path(@faculty)
  end

  protected
  def set_faculty
    @faculty = Faculty.find(params[:faculty_id])
  end

  def teacher_params
    params.require(:teacher).permit!
  end
end
