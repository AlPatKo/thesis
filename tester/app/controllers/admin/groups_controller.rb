class Admin::GroupsController < AdminController
  before_action :set_ancestors
  def new
    @group = @speciality.groups.new
  end

  def create
    @group = @speciality.groups.new group_params
    if @group.save
      redirect_to edit_admin_faculty_speciality_path(@faculty, @speciality)
    else
      render :new
    end
  end

  def edit
    @group = Group.find params[:id]
  end

  def update
    @group = Group.find params[:id]
    if @group.update group_params
      redirect_to edit_admin_faculty_speciality_path(@faculty, @speciality)
    else
      render :edit
    end
  end

  def destroy
    @group = Group.find params[:id]
    @group.destroy
    redirect_to edit_admin_faculty_speciality_path(@faculty, @speciality)
  end

  protected
  def set_ancestors
    @faculty = Faculty.find params[:faculty_id]
    @speciality = Speciality.find params[:speciality_id]
  end

  def group_params
    params.require(:group).permit!
  end
end
