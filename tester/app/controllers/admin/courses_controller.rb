class Admin::CoursesController < AdminController
  before_action :set_faculty
  
  def new
    @course = Course.new
  end

  def create
    @course = Course.new course_params
    if @course.save
      redirect_to edit_admin_faculty_path(@faculty)
    else
      render :new
    end
  end

  def edit
    @course = Course.find params[:id]
  end

  def update
    @course = Course.find params[:id]
    if @course.update course_params
      redirect_to edit_admin_faculty_path(@faculty)
    else
      render :edit
    end
  end

  def destroy
    @course = Course.find params[:id]
    @course.destroy
    redirect_to edit_admin_faculty_path(@faculty)
  end

  protected

  def set_faculty
    @faculty = Faculty.find(params[:faculty_id])
  end

  def course_params
    params.require(:course).permit!
  end
end
