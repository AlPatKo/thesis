class Admin::SpecialitiesController < AdminController
  before_action :set_faculty

  def new
    @speciality = @faculty.specialities.new
  end

  def create
    @speciality = @faculty.specialities.new speciality_params
    if @speciality.save
      redirect_to edit_admin_faculty_path(@faculty)
    else
      render :new
    end
  end

  def edit
    @speciality = Speciality.find params[:id]
  end

  def update
    @speciality = Speciality.find params[:id]
    if @speciality.update speciality_params
      redirect_to edit_admin_faculty_path(@faculty)
    else
      render :edit
    end
  end

  def destroy
    @speciality = Speciality.find params[:id]
    @speciality.destroy
    redirect_to edit_admin_faculty_path(@faculty)
  end

  protected

  def set_faculty
    @faculty = Faculty.find params[:faculty_id]
  end

  def speciality_params
    params.require(:speciality).permit!
  end
end
