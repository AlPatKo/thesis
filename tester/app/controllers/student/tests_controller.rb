class Student::TestsController < StudentController
  before_action :set_course
  def show
    @test = Test.find params[:id]
  end

  protected
  def set_course
    @course = Course.find params[:course_id]
  end
end
