class Student::CoursesController < StudentController
  def index
    @courses = current_student.group.courses
  end

  def show
    @course = Course.find params[:id]
  end
end
