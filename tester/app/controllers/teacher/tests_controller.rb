class Teacher::TestsController < TeacherController
  before_action :set_course
  def new
    @test = @course.tests.new
  end

  def create
    @test = @course.tests.new test_params
    if @test.save
      redirect_to edit_teacher_course_path(@course)
    else
      render :new
    end
  end

  def edit
    @test = Test.find params[:id]
  end

  def update
    @test = Test.find params[:id]
    if @test.update test_params
      redirect_to edit_teacher_course_path(@course)
    else
      render :edit
    end
  end

  def destroy
    Test.find(params[:id]).destroy
    redirect_to edit_teacher_course_path(@course)
  end

  protected

  def set_course
    @course = Course.find params[:course_id]
  end

  def test_params
    params.require(:test).permit!
  end
end
