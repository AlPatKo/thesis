class Teacher::CoursesController < TeacherController

  def index
    @courses = current_teacher.courses
  end

  def edit
    @course = Course.find params[:id]
  end

  def update
    @course = Course.find params[:id]
    if @course.update course_params
      redirect_to teacher_courses_path
    else
      render :edit
    end
  end

  protected

  def course_params
    params.require(:course).permit(:syllabus)
  end
end
