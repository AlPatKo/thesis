class Teacher::QuestionsController < TeacherController
  before_action :set_ancestors
  def new
    @question = @test.questions.new
  end

  def create
    @question = @test.questions.new question_params
    if @question.save
      redirect_to edit_teacher_course_test_path(@course, @test)
    else
      render :new
    end
  end

  def edit
    @question = Question.find params[:id]
  end

  def update
    @question = Question.find params[:id]
    if @question.update question_params
      redirect_to edit_teacher_course_test_path(@course, @test)
    else
      render :edit
    end
  end

  def destroy
    Question.find(params[:id]).destroy
    redirect_to edit_teacher_course_test_path(@course, @test)
  end

  protected
  def question_params
    params.require(:question).permit!
  end

  def set_ancestors
    @course = Course.find(params[:course_id])
    @test = Test.find params[:test_id]
  end
end
