class Teacher::AttemptsController < TeacherController
  before_action :set_ancestors
  def index
    @attempts = @test.attempts.group_by{|a| a.checked?}
  end

  def show
    @attempt = Attempt.find params[:id]
  end

  def edit
    @attempt = Attempt.find params[:id]
  end

  def update
    grades = params[:attempt]
    grades.each do |a_id, grade|
      Answer.find(a_id).update(points: grade[:scored], checked: true)
    end
    redirect_to teacher_course_test_attempts_path(@course, @test)
  end

  protected

  def set_ancestors
    @test = Test.find params[:test_id]
    @course = Course.find params[:course_id]
  end
end
