class Faculty < ActiveRecord::Base
  has_many :teachers
  has_many :courses, through: :teachers
  has_many :specialities, dependent: :destroy
  has_many :groups, through: :specialities
end
