class Test < ActiveRecord::Base
  belongs_to :course
  has_many :questions
  has_many :attempts

  def max_points
    max = 0
    self.questions.group_by{|q| q.order}.each do |o, qs|
      max += qs.first.worth
    end
    max
  end

  scope :visible, -> {where(visible: true)}
end
