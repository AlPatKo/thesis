class Group < ActiveRecord::Base
  belongs_to :speciality
  has_many :courses
  has_many :students
  has_many :tests, through: :courses

  enum language: [:romanian, :russian, :english, :french]
end
