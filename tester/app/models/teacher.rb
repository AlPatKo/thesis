class Teacher < ActiveRecord::Base
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :courses
  has_many :tests, through: :courses

  belongs_to :faculty

  def fullname
    "#{self.name} #{self.surname}"
  end
end
