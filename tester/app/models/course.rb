class Course < ActiveRecord::Base
  belongs_to :teacher
  belongs_to :group
  has_many :tests
end
