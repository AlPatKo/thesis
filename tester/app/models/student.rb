class Student < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :group
  has_many :courses, through: :group
  has_many :attempts

  def fullname
    "#{self.name} #{self.surname}"
  end

  def submitted_attempts(course)
    submitted = 0
    course.tests.each do |t|      
      submitted += 1 if t.attempts.map{|a| a.student_id}.include? id
    end
    submitted
  end
end
