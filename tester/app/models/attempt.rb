class Attempt < ActiveRecord::Base
  belongs_to :student
  belongs_to :test
  has_many :answers

  def checked?
    self.answers.each do |a|
      unless a.checked 
        return false
      end
    end
    return true
  end

  def total
    self.answers.map{|a| a.points}.reduce(:+)
  end
end
