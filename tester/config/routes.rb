Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :students, controllers: {
    registrations: 'student/registrations'
  }
  devise_for :teachers
  devise_for :admins
  
  root 'home#index'

  namespace :admin do
    root 'dashboard#index'
    resources :faculties, except: :show do
      resources :teachers, except: [:edit, :update, :index]
      resources :courses, except: [:show, :index]
      resources :specialities, except: [:show, :index] do
        resources :groups, except: [:index, :show]
      end
    end
  end

  namespace :teacher do
    root 'dashboard#index'
    resources :courses, only: [:index, :edit, :update] do
      resources :tests, except: [:index, :show] do
        resources :attempts, only: [:edit, :update, :index, :show]
        resources :questions, except: [:index, :show]
      end
    end
  end

  namespace :student do
    root 'dashboard#index'
    resources :courses, only: [:index, :show] do
      resources :tests, only: [:show] do 
        resources :attempts, only: [:new, :create]
      end
    end
  end
end
